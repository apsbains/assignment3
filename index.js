const balanceElement = document.getElementById("balance")
const loanTextElement = document.getElementById("loan")
const loanAmountElement = document.getElementById("loan-amount")
const loanButtonElement = document.getElementById("get-loan")

const payElement = document.getElementById("pay")
const bankButtonElement = document.getElementById("transfer-money")
const workButtonELement = document.getElementById("work1")

const paybackLoanElement = document.getElementById("payback-loan")

const computersElement = document.getElementById("komputers")
const computerSpecsElement = document.getElementById("komputer-features")
const computerDetailsElement = document.getElementById("computer-details")

let balance = 0
let loan = 0
let loanCounter = 0
let pay = 0 
let computers = []
let computerSpecs = []

balanceElement.innerText = `${balance}`
payElement.innerText = `${pay} Kr`
let endPoint = "https://noroff-komputer-store-api.herokuapp.com/"
let apiUrl = "https://noroff-komputer-store-api.herokuapp.com/computers" 


fetch(apiUrl)
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToMenu(computers))

const addComputersToMenu = (computers) => {
    computers.forEach(x => addComputerToMenu(x))
    computerSpecs = computers[0].specs
    addListElements(computerSpecs)
    addComputerDetails(computers[0])
}

const addComputerDetails = (computer) => {
        
    const computerImageCol =  document.createElement("div")
    computerImageCol.setAttribute('class', 'col')
    
    const computerImage = document.createElement("img") 
    const imageSrc = endPoint+computer.image
    computerImage.src = imageSrc
    computerImage.setAttribute('style', 'width: 50%')
    
    computerImageCol.appendChild(computerImage)
    computerDetailsElement.appendChild(computerImageCol)
    
    const computerCol1 =  document.createElement("div")
    computerCol1.setAttribute('class', 'col')
    
    const computerRow1 = document.createElement("div")
    computerRow1.setAttribute('class', 'row')
    const pTag = document.createElement('p') 
    pTag.innerText = computer.title
    pTag.setAttribute('style','font-weight: bold')
    computerRow1.setAttribute('style','text-align:left')
    computerRow1.appendChild(pTag)
    

    const computerRow2 = document.createElement("div")
    computerRow2.setAttribute('class','row')    
    computerRow2.setAttribute('style','text-align:left')
    computerRow2.innerText = computer.description
        
    
    const computerRowIMellom = document.createElement("p")
    //computerRowIMellom.setAttribute('class','p')        
    computerRowIMellom.innerText = ` `

    computerCol1.appendChild(computerRow1)
    computerCol1.appendChild(computerRowIMellom)
    computerCol1.appendChild(computerRow2)    
    computerDetailsElement.appendChild(computerCol1)
    
    const computerCol2 =  document.createElement("div")
    computerCol2.setAttribute('class', 'col')
    
    const computerRow21 = document.createElement("div")
    computerRow21.setAttribute('class', 'row')
    computerRow21.setAttribute('style','text-align:left')    
    const spanTag = document.createElement('p') 
    spanTag.innerText = computer.price + " NOK"
    spanTag.setAttribute('style','font-weight: bold')    
    computerRow21.appendChild(spanTag)
    
    const computerButton = document.createElement("button")
    computerButton.innerText = "Buy now"
    computerButton.setAttribute('class', 'btn btn-primary')
    computerButton.setAttribute('style','text-align:left')
    
    computerButton.addEventListener("click", () =>{
        if (computer.price > balance) {
            alert('You cannot afford this laptop')
        }else if(balance >= computer.price) {
            balance = balance - parseInt(computer.price)
            balanceElement.innerText = `${balance}`
            alert(`You are a owner of ${computer.title}`)
        }
    })
    
    computerCol2.appendChild(computerRow21)
    computerCol2.appendChild(computerButton)
    computerDetailsElement.appendChild(computerCol2)
    

}

const addComputerToMenu = (computer) => {
    const computerElement = document.createElement("option")
    computerElement.value = computer.id
    computerElement.appendChild(document.createTextNode(computer.title))
    computersElement.appendChild(computerElement)     
}
const handleComputerMenuChange = e => {

    const listElements= document.getElementById(`komputer-features`)
    listElements.innerHTML = `Features`  
    const selectedComputer = computers[e.target.selectedIndex]
    computerSpecs = selectedComputer.specs    
    addListElements(computerSpecs)
    computerDetailsElement.innerHTML = ``
    addComputerDetails(selectedComputer)   
}

const addListElements = (computerSpecs) => {
    for (const spec in computerSpecs) {        
        const specElement = document.createElement("li")
        specElement.innerText = computerSpecs[spec]        
        computerSpecsElement.appendChild(specElement)
    }
}

const createpayBackLoanButton = () => {
    
    if( loan > 0 ) {
        
        const createButtonElement = document.createElement("button")        
        createButtonElement.innerText = `Payback loan`        
        createButtonElement.setAttribute('class', 'btn btn-primary')
        createButtonElement.setAttribute('id', `payback`)
        paybackLoanElement.appendChild(createButtonElement)        
        createButtonElement.addEventListener("click", ()=> {
            if(pay > 0) {
                payBackLoan(pay)
                pay = 0
                payElement.innerText = `${pay} Kr`
                if(loan > 0 ) {
                    loanTextElement.innerText = `Loan: `
                    loanAmountElement.innerText = `${loan}`
                } else {
                    loanTextElement.innerText = ``
                    loanAmountElement.innerText = ``            
                    document.getElementById(`payback-loan`).removeChild(createButtonElement)

                    alert(`You have paid the loan back`)            
                }
                    
            } else {
                alert(`Your pay is ${pay}, Nothing to payback, Please work...`)            
            }
            
        })        
    }    
}

const payBackLoan = (pay) => {
    
    if(loan >= 0 && pay > 0) {
        
        if(loan >= pay) {
            loan = loan - pay
            pay = 0 
            payElement.innerText = `${pay} Kr`               
            loanAmountElement.innerText = `${loan}`
            if(loan == 0) {
                loanCounter = 0
                loanTextElement.innerText = ``
                loanAmountElement.innerText = ``
            }             
        } else {
            const surPLusAmount = pay - loan
            balance += surPLusAmount
            loan = 0
            loanCounter = 0            
            loanTextElement.innerText = ``
            loanAmountElement.innerText = ``
            balanceElement.innerText = `${balance}`

        }
    }
}

const handleGetLoan = () => {
    if(loanCounter > 0 ) {
        alert(`You are only allowed to have a loan once and when you bought computer then you can have another loan`)
    }
    if (loanCounter == 0 ) {
        if(balance > 0 ) {
    
            const loanAmount =  prompt("Please enter the amount you want to loan")
            
            if(parseInt(loanAmount) <= balance*2 ) {
                loan += parseInt(loanAmount)
                if (loan != 0 ) {
                    loanCounter = 1
                    loanTextElement.innerText = `Loan: `
                    loanAmountElement.innerText = `${loan}`
                    createpayBackLoanButton()
                }
            } else {
                alert("You cannot get a loan more than double of your bank balance")    
            }            
        }else {
            alert(`You are not alowed to have a loan since your balance is ${balance}`)
        }
    }
    
}

const handleWork = () => {

    //console.log(` Pay inside handleWork is ${pay}`)    
    pay += 100
    payElement.innerText = `${pay} Kr`


}

const handleTransferMoney = () => {
    
    if(pay > 0 ){
        if(loan > 0) {
            const tenProsent = parseInt(pay/10)
            payBackLoan(tenProsent)
            console.log(`Inside handleTransferMoney else ${tenProsent}`)            
        }
        
        balance += pay
        pay = 0
        
        payElement.innerText = `${pay} Kr`
        balanceElement.innerText = `${balance}`

    }else {        
        alert(`Nothing to Transfer`)
    }
}

const handleBuyComputer = (computer) => {
    
    /*if (computer.price > balance) {
        alert('You cannot afford this laptop')
    }else if(balance >= computer.price) {
        balance = balance - parseInt(computer.price)
        balanceElement.innerText = `${balance}`
        alert(`You are a owner of ${computer.title}`)
    }*/
    console.log(`price is new ${computer.price}`)

    
}

loanButtonElement.addEventListener("click", handleGetLoan)
workButtonELement.addEventListener("click", handleWork)
bankButtonElement.addEventListener("click", handleTransferMoney)
computersElement.addEventListener("change", handleComputerMenuChange)
//computerButton.addEventListener("click",handleBuyComputer(computer))